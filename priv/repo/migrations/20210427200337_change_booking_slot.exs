defmodule Bakery.Repo.Migrations.ChangeBookingSlot do
  use Ecto.Migration

  alias Bakery.Repo
  alias Bakery.BookingSlots.BookingSlot

  def up do
    alter table(:booking_slots) do
      add :time_slot, :integer, default: 0
      modify :datetime, :date
    end

    flush()

    rename table(:booking_slots), :datetime, to: :date
  end
end
