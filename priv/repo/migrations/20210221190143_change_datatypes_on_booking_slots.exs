defmodule Bakery.Repo.Migrations.ChangeDatatypesOnBookingSlots do
  use Ecto.Migration

  def change do
    alter table(:booking_slots) do
      remove :date
      remove :time_slot
      remove :booked
      add :datetime, :utc_datetime
    end
  end
end
