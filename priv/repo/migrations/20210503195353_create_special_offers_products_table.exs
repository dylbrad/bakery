defmodule Bakery.Repo.Migrations.CreateSpecialOffersProductsTable do
  use Ecto.Migration

  def change do
    create table(:special_offers_products) do
      add :special_offer_id, references(:special_offers, on_delete: :nothing)
      add :product_id, references(:products, on_delete: :nothing)
    end
    create index(:special_offers_products, [:special_offer_id])
    create index(:special_offers_products, [:product_id])
  end
end
