defmodule Bakery.Repo.Migrations.CreateImages do
  use Ecto.Migration

  def change do
    create table(:images) do
      add :filename, :text

      timestamps()
    end

  end
end
