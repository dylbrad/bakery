defmodule Bakery.Repo.Migrations.CreateProducts do
  use Ecto.Migration

  def change do
    create table(:products) do
      add :name, :text
      add :key, :text
      add :description, :text
      add :image, :text
      add :price, :float

      timestamps()
    end

    create unique_index(:products, [:key])
  end
end
