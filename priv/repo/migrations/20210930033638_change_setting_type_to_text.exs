defmodule Bakery.Repo.Migrations.ChangeSettingTypeToText do
  use Ecto.Migration

  def change do
    alter table(:settings) do
      modify :value, :text
    end
  end
end
