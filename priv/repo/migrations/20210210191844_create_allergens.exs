defmodule Bakery.Repo.Migrations.CreateAllergens do
  use Ecto.Migration

  def change do
    create table(:allergens) do
      add :name, :string
      add :image, :string

      timestamps()
    end

  end
end
