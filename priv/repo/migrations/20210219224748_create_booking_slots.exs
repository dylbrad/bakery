defmodule Bakery.Repo.Migrations.CreateBookingSlots do
  use Ecto.Migration

  def change do
    create table(:booking_slots) do
      add :date, :date
      add :time_slot, :integer
      add :booked, :boolean, default: false, null: false
      add :order_id, references(:orders, on_delete: :nothing, type: :binary_id)

      timestamps()
    end

    create index(:booking_slots, [:order_id])
  end
end
