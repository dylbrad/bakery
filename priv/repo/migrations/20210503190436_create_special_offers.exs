defmodule Bakery.Repo.Migrations.CreateSpecialOffers do
  use Ecto.Migration

  def change do
    create table(:special_offers) do
      add :name, :string
      add :order, :integer
      add :disabled, :boolean, default: true, null: false
      add :price, :float
      add :image, :text

      timestamps()
    end

  end
end
