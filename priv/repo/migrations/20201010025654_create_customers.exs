defmodule Bakery.Repo.Migrations.CreateCustomers do
  use Ecto.Migration

  def change do
    create table(:customers) do
      add :first_name, :text
      add :last_name, :text
      add :email, :text
      add :phone, :text
      add :address1, :text
      add :address2, :text
      add :city, :text
      add :county, :text

      timestamps()
    end

    create unique_index(:customers, [:email])
  end
end
