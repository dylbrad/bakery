defmodule Bakery.Repo.Migrations.CreateTrolleys do
  use Ecto.Migration

  def change do
    create table(:trolleys) do
      add :delivery_price, :float

      timestamps()
    end

  end
end
