defmodule Bakery.Repo.Migrations.MoveCustomerDataToCustomersTable do
  use Ecto.Migration
  import Ecto.Query, warn: false
  alias Bakery.Orders.Order
  alias Bakery.Customers.Customer
  alias Bakery.Repo

  def up do
    drop unique_index(:customers, [:email])

    flush()

    # Move data from deliveries table to customers table
    orders = from(order in "orders", select: [order.id, order.delivery_id])
    |> Repo.all()
    |> Enum.map(fn [order_id, delivery_id] ->
      delivery = from(
        delivery in "deliveries",
        select: [
          delivery.id,
          delivery.first_name,
          delivery.last_name,
          delivery.phone,
          delivery.email,
          delivery.address1,
          delivery.address2,
          delivery.city,
          delivery.county,
        ],
        where: delivery.id == ^delivery_id
      )
      |> Repo.one()

      customer = Repo.insert!(%Customer{
        first_name: Enum.at(delivery, 1),
        last_name: Enum.at(delivery, 2),
        phone: Enum.at(delivery, 3),
        email: Enum.at(delivery, 4),
        address1: Enum.at(delivery, 5),
        address2: Enum.at(delivery, 6),
        city: Enum.at(delivery, 7),
        county: Enum.at(delivery, 8)
      })
      from(order in "orders", where: order.id == ^order_id)
      |> Repo.update_all(set: [customer_id: customer.id])
    end)

    alter table("deliveries") do
      remove :address1
      remove :address2
      remove :city
      remove :county
      remove :email
      remove :first_name
      remove :last_name
      remove :phone
    end
  end
end
