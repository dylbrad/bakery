defmodule Bakery.Repo.Migrations.AddPaymentDataToOrders do
  use Ecto.Migration
  import Ecto.Query, warn: false
  alias Bakery.Payments.Payment
  alias Bakery.Repo

  def up do
    from(order in "orders", where: is_nil(order.payment_id), select: order.id)
    |> Repo.all()
    |> Enum.map(fn order_id ->
      payment = %Payment{type: "cash"}
      |> Repo.insert!

      from(order in "orders",
        update: [set: [payment_id: ^payment.id]],
        where: order.id == ^order_id)
        |> Repo.update_all([])
    end)
  end
end
