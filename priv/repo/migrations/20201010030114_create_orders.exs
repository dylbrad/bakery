defmodule Bakery.Repo.Migrations.CreateOrders do
  use Ecto.Migration

  def change do
    create table(:orders, primary_key: false) do
      add :id, :uuid, primary_key: true
      add :status_id, references(:order_statuses, on_delete: :nothing)
      add :delivery_id, references(:deliveries, on_delete: :nothing)
      add :trolley_id, references(:trolleys, on_delete: :nothing)
      add :customer_id, references(:customers, on_delete: :nothing)

      timestamps()
    end

    create index(:orders, [:status_id])
    create index(:orders, [:delivery_id])
    create index(:orders, [:trolley_id])
    create index(:orders, [:customer_id])
  end
end
