defmodule Bakery.Repo.Migrations.ChangeDeliveryDateOnDeliveries do
  use Ecto.Migration

  def change do
    alter table(:deliveries) do
      modify :delivery_date, :date
      add :time_slot, :integer
    end
  end
end
