defmodule Bakery.Repo.Migrations.CreateRecipes do
  use Ecto.Migration

  def change do
    create table(:recipes) do
      add :recipe, :text
      add :product_id, references(:products, on_delete: :nothing)

      timestamps()
    end

    create index(:recipes, [:product_id])
  end
end
