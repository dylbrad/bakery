defmodule Bakery.Repo.Migrations.CreateConfirmationKeys do
  use Ecto.Migration

  def change do
    create table(:confirmation_keys) do
      add :confirmation_key, :text
      add :expires_on, :utc_datetime
      add :order_id, references(:orders, on_delete: :nothing, type: :binary_id)

      timestamps()
    end

    create index(:confirmation_keys, [:order_id])
  end
end
