# Bakery

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.setup`
  * Install Node.js dependencies with `npm install` inside the `assets` directory
  * Setup the upload folder with `mix setup_upload`
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

Ready to run in production? Please [check our deployment guides](https://hexdocs.pm/phoenix/deployment.html).

## Learn more

  * Official website: https://www.phoenixframework.org/
  * Guides: https://hexdocs.pm/phoenix/overview.html
  * Docs: https://hexdocs.pm/phoenix
  * Forum: https://elixirforum.com/c/phoenix-forum
  * Source: https://github.com/phoenixframework/phoenix



## Environment variables

| Environment variable   | Description               | Example       | Default value |
|------------------------|---------------------------|---------------| --------------|
| MAILGUN_DOMAIN         | Mailgun domain name       | https://api.mailgun.net/v3/sandbox7d4a39.mailgun.org | |
| MAILGUN_KEY            | Mailgun key               | 813b7a0bc1213e5d-ee1bae71-3c36686e | |
| MAILGUN_FROM_EMAIL     | Mailgun `from` email      | no-reply@example.org | |
| UPLOAD_FOLDER          | Path of the upload folder | `/path/to/upload` | `<Path of the project>/../uploads` |
| BAKERY_URL             | This project URL          | example.org       | http://localhost:4000 |
| ADMIN_URL              | The admin app URL          | example.org       | http://localhost:3001 |


## Deployment

Deploy to production with:

    ./bin/deploy.sh bakery 4001


Deploy to testing with:

    ./bin/deploy.sh bakery-testing 4002
