defmodule Bakery.DateFormat do
  def format_date(date) do
    Timex.format!(date, "%d/%m/%Y", :strftime)
  end
end
