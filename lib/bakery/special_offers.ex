defmodule Bakery.SpecialOffers do
  @moduledoc """
  The SpecialOffers context.
  """

  import Ecto.Query, warn: false
  alias Bakery.Repo
  alias Bakery.Products

  alias Bakery.SpecialOffers.SpecialOffer

  @doc """
  Returns the list of special_offers.

  ## Examples

      iex> list_special_offers()
      [%SpecialOffer{}, ...]

  """
  def list_special_offers() do
    SpecialOffer
    |> where([special_offer], not special_offer.disabled)
    |> order_by([special_offer], special_offer.order)
    |> Repo.all()
    |> Repo.preload([:products])
  end

  def list_special_offers(:all) do
    SpecialOffer
    |> order_by([special_offer], special_offer.order)
    |> Repo.all()
    |> Repo.preload([:products])
  end

  @doc """
  Gets a single special_offer.

  Raises `Ecto.NoResultsError` if the Special offer does not exist.

  ## Examples

      iex> get_special_offer!(123)
      %SpecialOffer{}

      iex> get_special_offer!(456)
      ** (Ecto.NoResultsError)

  """
  def get_special_offer!(id) do
    SpecialOffer
    |> Repo.get!(id)
    |> Repo.preload([:products])
  end

  @doc """
  Creates a special_offer.

  ## Examples

      iex> create_special_offer(%{field: value})
      {:ok, %SpecialOffer{}}

      iex> create_special_offer(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_special_offer(attrs \\ %{}) do
    products = (attrs["products"] || Map.get(attrs , :products, []))
    |> Products.get_products!()

    %SpecialOffer{}
    |> SpecialOffer.changeset(attrs)
    |> Ecto.Changeset.put_assoc(:products, products)
    |> Repo.insert()
  end

  @doc """
  Updates a special_offer.

  ## Examples

      iex> update_special_offer(special_offer, %{field: new_value})
      {:ok, %SpecialOffer{}}

      iex> update_special_offer(special_offer, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_special_offer(%SpecialOffer{} = special_offer, attrs) do
    products = (attrs["products"] || Map.get(attrs , :products, []))
    |> Products.get_products!()

    special_offer
    |> SpecialOffer.changeset(attrs)
    |> Ecto.Changeset.put_assoc(:products, products)
    |> Repo.update()
  end

  @doc """
  Deletes a special_offer.

  ## Examples

      iex> delete_special_offer(special_offer)
      {:ok, %SpecialOffer{}}

      iex> delete_special_offer(special_offer)
      {:error, %Ecto.Changeset{}}

  """
  def delete_special_offer(%SpecialOffer{} = special_offer) do
    Repo.delete(special_offer)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking special_offer changes.

  ## Examples

      iex> change_special_offer(special_offer)
      %Ecto.Changeset{data: %SpecialOffer{}}

  """
  def change_special_offer(%SpecialOffer{} = special_offer, attrs \\ %{}) do
    SpecialOffer.changeset(special_offer, attrs)
  end
end
