defmodule Bakery.Payments.Payment do
  use Ecto.Schema
  import Ecto.Changeset

  schema "payments" do
    field :transaction_code, :string
    field :type, :string

    timestamps()
  end

  @doc false
  def changeset(payment, attrs) do
    payment
    |> cast(attrs, [:type])
    |> validate_required([:type])
  end

  @doc false
  def admin_changeset(payment, attrs) do
    payment
    |> cast(attrs, [:type, :transaction_code])
    |> validate_required([:type])
  end
end
