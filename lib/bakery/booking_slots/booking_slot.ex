defmodule Bakery.BookingSlots.BookingSlot do
  use Ecto.Schema
  import Ecto.Changeset

  alias Bakery.Orders.Order

  schema "booking_slots" do
    field :date, :date
    field :time_slot, :integer
    belongs_to :order, Order, type: :binary_id
    field :is_delivery, :boolean, default: true

    timestamps()
  end

  @doc false
  def changeset(booking_slot, attrs) do
    booking_slot
    |> cast(attrs, [:date, :time_slot, :order_id, :is_delivery])
    |> validate_required([:date, :time_slot, :is_delivery])
  end
end
