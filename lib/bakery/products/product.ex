defmodule Bakery.Products.Product do
  use Ecto.Schema
  import Ecto.Changeset

  alias Bakery.Allergens

  schema "products" do
    field :description, :string
    field :image, :string
    field :key, :string
    field :name, :string
    field :price, :float
    field :disabled, :boolean, default: true
    field :order, :integer
    many_to_many :allergens, Allergens.Allergen,
      join_through: "products_allergens",
      on_delete: :delete_all,
      on_replace: :delete
    many_to_many :may_contain_allergens, Allergens.Allergen,
      join_through: "products_may_contain_allergens",
      on_delete: :delete_all,
      on_replace: :delete

    timestamps()
  end

  @doc false
  def changeset(product, attrs) do
    product
    |> cast(attrs, [:name, :key, :description, :image, :price, :disabled, :order])
    |> validate_required([:name, :key, :description, :image, :price])
  end
end
