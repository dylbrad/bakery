defmodule Bakery.Allergens.Allergen do
  use Ecto.Schema
  import Ecto.Changeset

  schema "allergens" do
    field :image, :string
    field :name, :string

    timestamps()
  end

  @doc false
  def changeset(allergen, attrs) do
    allergen
    |> cast(attrs, [:name, :image])
    |> validate_required([:name, :image])
  end
end
