defmodule Bakery.Messages.Message do
  use Ecto.Schema
  import Ecto.Changeset

  schema "messages" do
    field :email, :string
    field :message, :string
    field :name, :string

    timestamps()
  end

  @doc false
  def changeset(message, attrs) do
    message
    |> cast(attrs, [:name, :email, :message])
    |> validate_required([:name, :email, :message])
  end
end
