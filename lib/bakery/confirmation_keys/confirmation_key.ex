defmodule Bakery.ConfirmationKeys.ConfirmationKey do
  use Ecto.Schema
  import Ecto.Changeset

  schema "confirmation_keys" do
    field :confirmation_key, :string
    field :expires_on, :utc_datetime
    belongs_to :order, Bakery.Orders.Order, type: :binary_id

    timestamps()
  end

  @doc false
  def changeset(confirmation_key, attrs) do
    confirmation_key
    |> cast(attrs, [:confirmation_key, :expires_on, :order_id])
    |> validate_required([:confirmation_key, :expires_on, :order_id])
  end
end
