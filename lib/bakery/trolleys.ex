defmodule Bakery.Trolleys do
  @moduledoc """
  The Trolleys context.
  """

  import Ecto.Query, warn: false
  alias Bakery.Repo

  alias Bakery.Trolleys.Trolley

  @doc """
  Returns the list of trolleys.

  ## Examples

      iex> list_trolleys()
      [%Trolley{}, ...]

  """
  def list_trolleys do
    Repo.all(Trolley)
    |> Repo.preload([:items])
  end

  @doc """
  Gets a single trolley.

  Raises `Ecto.NoResultsError` if the Trolley does not exist.

  ## Examples

      iex> get_trolley!(123)
      %Trolley{}

      iex> get_trolley!(456)
      ** (Ecto.NoResultsError)

  """
  def get_trolley!(id) do
    Repo.get!(Trolley, id)
    |> Repo.preload([:items])
  end

  @doc """
  Creates a trolley.

  ## Examples

      iex> create_trolley(%{field: value})
      {:ok, %Trolley{}}

      iex> create_trolley(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_trolley(attrs \\ %{}) do
    %Trolley{}
    |> Repo.preload([:items])
    |> Trolley.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a trolley.

  ## Examples

      iex> update_trolley(trolley, %{field: new_value})
      {:ok, %Trolley{}}

      iex> update_trolley(trolley, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_trolley(%Trolley{} = trolley, attrs) do
    trolley
    |> Trolley.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a trolley.

  ## Examples

      iex> delete_trolley(trolley)
      {:ok, %Trolley{}}

      iex> delete_trolley(trolley)
      {:error, %Ecto.Changeset{}}

  """
  def delete_trolley(%Trolley{} = trolley) do
    Repo.delete(trolley)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking trolley changes.

  ## Examples

      iex> change_trolley(trolley)
      %Ecto.Changeset{data: %Trolley{}}

  """
  def change_trolley(%Trolley{} = trolley, attrs \\ %{}) do
    Trolley.changeset(trolley, attrs)
  end


  def calculate_trolley_total(%Trolley{} = trolley) do
    items_price = trolley.items
    |> Enum.map(fn item -> item.unit_price * item.quantity end)
    |> Enum.sum()

    items_price = (items_price / 1)
    |> Float.round(2)

    items_price + trolley.delivery_price
  end

  alias Bakery.Trolleys.TrolleyItem

  @doc """
  Returns the list of trolley_items.

  ## Examples

      iex> list_trolley_items()
      [%TrolleyItem{}, ...]

  """
  def list_trolley_items do
    Repo.all(TrolleyItem)
  end

  @doc """
  Gets a single trolley_item.

  Raises `Ecto.NoResultsError` if the Trolley item does not exist.

  ## Examples

      iex> get_trolley_item!(123)
      %TrolleyItem{}

      iex> get_trolley_item!(456)
      ** (Ecto.NoResultsError)

  """
  def get_trolley_item!(id) do
    Repo.get!(TrolleyItem, id)
  end

  @doc """
  Creates a trolley_item.

  ## Examples

      iex> create_trolley_item(%{field: value})
      {:ok, %TrolleyItem{}}

      iex> create_trolley_item(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_trolley_item(attrs \\ %{}) do
    %TrolleyItem{}
    |> TrolleyItem.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a trolley_item.

  ## Examples

      iex> update_trolley_item(trolley_item, %{field: new_value})
      {:ok, %TrolleyItem{}}

      iex> update_trolley_item(trolley_item, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_trolley_item(%TrolleyItem{} = trolley_item, attrs) do
    trolley_item
    |> TrolleyItem.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a trolley_item.

  ## Examples

      iex> delete_trolley_item(trolley_item)
      {:ok, %TrolleyItem{}}

      iex> delete_trolley_item(trolley_item)
      {:error, %Ecto.Changeset{}}

  """
  def delete_trolley_item(%TrolleyItem{} = trolley_item) do
    Repo.delete(trolley_item)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking trolley_item changes.

  ## Examples

      iex> change_trolley_item(trolley_item)
      %Ecto.Changeset{data: %TrolleyItem{}}

  """
  def change_trolley_item(%TrolleyItem{} = trolley_item, attrs \\ %{}) do
    TrolleyItem.changeset(trolley_item, attrs)
  end

  def calculate_trolley_item_price(%TrolleyItem{} = trolley_item) do
    Float.round(trolley_item.quantity * trolley_item.unit_price, 2)
  end
end
