defmodule Bakery.Recipes.Recipe do
  use Ecto.Schema
  import Ecto.Changeset

  alias Bakery.Products.Product

  schema "recipes" do
    field :recipe, :string, default: ""
    belongs_to :product, Product

    timestamps()
  end

  @doc false
  def create_changeset(recipe, attrs) do
    recipe
    |> cast(attrs, [:product_id, :recipe])
  end

  @doc false
  def changeset(recipe, attrs) do
    recipe
    |> cast(attrs, [:recipe])
  end
end
