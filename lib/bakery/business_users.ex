defmodule Bakery.BusinessUsers do
  @moduledoc """
  The BusinessUsers context.
  """

  import Ecto.Query, warn: false
  alias Bakery.Repo

  alias Bakery.BusinessUsers.BusinessUser

  @doc """
  Returns the list of business_users.

  ## Examples

      iex> list_business_users()
      [%BusinessUser{}, ...]

  """
  def list_business_users do
    Repo.all(BusinessUser)
  end

  @doc """
  Gets a single business_user.

  Raises `Ecto.NoResultsError` if the Business user does not exist.

  ## Examples

      iex> get_business_user!(123)
      %BusinessUser{}

      iex> get_business_user!(456)
      ** (Ecto.NoResultsError)

  """
  def get_business_user!(id), do: Repo.get!(BusinessUser, id)

  @doc """
  Creates a business_user.

  ## Examples

      iex> create_business_user(%{field: value})
      {:ok, %BusinessUser{}}

      iex> create_business_user(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_business_user(attrs \\ %{}) do
    %BusinessUser{}
    |> BusinessUser.changeset(attrs)
    |> Ecto.Changeset.put_change(:hashed_password, Bcrypt.hash_pwd_salt(attrs["password"]))
    |> Repo.insert()
  end

  @doc """
  Updates a business_user.

  ## Examples

      iex> update_business_user(business_user, %{field: new_value})
      {:ok, %BusinessUser{}}

      iex> update_business_user(business_user, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_business_user(%BusinessUser{} = business_user, attrs) do
    business_user
    |> BusinessUser.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a business_user.

  ## Examples

      iex> delete_business_user(business_user)
      {:ok, %BusinessUser{}}

      iex> delete_business_user(business_user)
      {:error, %Ecto.Changeset{}}

  """
  def delete_business_user(%BusinessUser{} = business_user) do
    Repo.delete(business_user)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking business_user changes.

  ## Examples

      iex> change_business_user(business_user)
      %Ecto.Changeset{data: %BusinessUser{}}

  """
  def change_business_user(%BusinessUser{} = business_user, attrs \\ %{}) do
    BusinessUser.changeset(business_user, attrs)
  end
end
