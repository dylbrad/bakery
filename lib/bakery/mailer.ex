defmodule Bakery.Mailer do

  require Logger

  use Mailgun.Client,
    domain: Application.get_env(:bakery, :mailgun_domain),
    key: Application.get_env(:bakery, :mailgun_key)

  def send_email(recipient, subject, content) do
    send_email to: recipient,
      from: Application.get_env(:bakery, :mailgun_from_email),
      subject: subject,
      html: content
    Logger.info("Email sent to #{recipient} with subject: #{subject}")
  end

  def send_confirmation_email(%{:is_delivery => true} = order, confirmation_order) do
    email_content = Phoenix.View.render_to_string(
      BakeryWeb.MailerView,
      "delivery_confirmation_email.html",
      layout: {BakeryWeb.LayoutView, "email.html"},
      order: order,
      now: DateTime.utc_now,
      confirmation_order: confirmation_order
    )
    send_email(order.customer.email, "Please confirm your order (##{order.id}) from Francois' Bakery", email_content)
  end

  def send_confirmation_email(%{:is_delivery => false} = order, confirmation_order) do
    email_content = Phoenix.View.render_to_string(
      BakeryWeb.MailerView,
      "collection_confirmation_email.html",
      layout: {BakeryWeb.LayoutView, "email.html"},
      order: order,
      now: DateTime.utc_now,
      confirmation_order: confirmation_order
    )
    send_email(order.customer.email, "Please confirm your order (##{order.id}) from Francois' Bakery", email_content)
  end

  def send_approved_order_email(order) do
    email_content = Phoenix.View.render_to_string(
      BakeryWeb.MailerView,
      "approved_order_email.html",
      layout: {BakeryWeb.LayoutView, "email.html"},
      order: order
    )
    send_email(order.customer.email, "Your order (##{order.id}) has been approved", email_content)
  end

  def send_declined_order_email(order) do
    email_content = Phoenix.View.render_to_string(
      BakeryWeb.MailerView,
      "declined_order_email.html",
      layout: {BakeryWeb.LayoutView, "email.html"},
      order: order
    )
    send_email(order.customer.email, "Your order (##{order.id}) has been declined", email_content)
  end

  def send_confirmation_email_to_baker(order, baker_email) do
    email_content = Phoenix.View.render_to_string(
      BakeryWeb.MailerView,
      "confirmation_email_baker.html",
      layout: {BakeryWeb.LayoutView, "email.html"},
      order: order,
      admin_url: Application.get_env(:bakery, :admin_url)
    )
    send_email(baker_email, "An order has been confirmed (##{order.id}) by a customer", email_content)
  end

  def send_message_received_email_to_baker(message, baker_email) do
    email_content = Phoenix.View.render_to_string(
      BakeryWeb.MailerView,
      "message_received_baker.html",
      layout: {BakeryWeb.LayoutView, "email.html"},
      message: message,
      admin_url: Application.get_env(:bakery, :admin_url)
    )
    send_email(baker_email, "A message has been received", email_content)
  end
end
