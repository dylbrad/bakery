defmodule Bakery.Products do
  @moduledoc """
  The Products context.
  """

  import Ecto.Query, warn: false
  alias Bakery.Repo

  alias Bakery.Products.Product
  alias Bakery.Recipes
  alias Bakery.Allergens

  @doc """
  Returns the list of products.

  ## Examples

      iex> list_products()
      [%Product{}, ...]

  """
  def list_products do
    Product
    |> where([product], not product.disabled)
    |> order_by([product], product.order)
    |> Repo.all()
    |> Repo.preload([:allergens, :may_contain_allergens])
  end

  def list_products(:all) do
    Product
    |> order_by([product], product.order)
    |> Repo.all()
    |> Repo.preload([:allergens, :may_contain_allergens])
  end

  def get_products!(ids) do
    ids
    |> Enum.map(fn id -> Repo.get(Product, id) end)
  end


  @doc """
  Gets a single product.

  Raises `Ecto.NoResultsError` if the Product does not exist.

  ## Examples

      iex> get_product!(123)
      %Product{}

      iex> get_product!(456)
      ** (Ecto.NoResultsError)

  """
  def get_product!(id) do
    Product
    |> Repo.get!(id)
    |> Repo.preload([:allergens, :may_contain_allergens])
  end

  @doc """
  Creates a product.

  ## Examples

      iex> create_product(%{field: value})
      {:ok, %Product{}}

      iex> create_product(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_product(attrs \\ %{}) do
    allergens = (attrs["allergens"] || Map.get(attrs , :allergens, []))
    |> Allergens.get_allergens!()

    may_contain_allergens = (
      attrs["may_contain_allergens"]
      || Map.get(attrs , :may_contain_allergens, [])
    )
    |> Allergens.get_allergens!()

    {:ok, product} = %Product{}
    |> Product.changeset(attrs)
    |> Ecto.Changeset.put_assoc(:allergens, allergens)
    |> Ecto.Changeset.put_assoc(:may_contain_allergens, may_contain_allergens)
    |> Repo.insert()

    {:ok, _recipe} = Recipes.create_recipe(%{product_id: product.id, recipe: ""})
    {:ok, product}
  end

  def create_product!(attrs \\ %{}) do
    %Product{}
    |> Product.changeset(attrs)
    |> Repo.insert!()
  end

  @doc """
  Updates a product.

  ## Examples

      iex> update_product(product, %{field: new_value})
      {:ok, %Product{}}

      iex> update_product(product, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_product(%Product{} = product, attrs) do
    allergens = (attrs["allergens"] || Map.get(attrs , :allergens, []))
    |> Allergens.get_allergens!()

    may_contain_allergens = (
      attrs["may_contain_allergens"]
      || Map.get(attrs , :may_contain_allergens, [])
    )
    |> Allergens.get_allergens!()

    product
    |> Product.changeset(attrs)
    |> Ecto.Changeset.put_assoc(:allergens, allergens)
    |> Ecto.Changeset.put_assoc(:may_contain_allergens, may_contain_allergens)
    |> Repo.update()
  end

  @doc """
  Deletes a product.

  ## Examples

      iex> delete_product(product)
      {:ok, %Product{}}

      iex> delete_product(product)
      {:error, %Ecto.Changeset{}}

  """
  def delete_product(%Product{} = product) do
    Repo.delete(product)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking product changes.

  ## Examples

      iex> change_product(product)
      %Ecto.Changeset{data: %Product{}}

  """
  def change_product(%Product{} = product, attrs \\ %{}) do
    Product.changeset(product, attrs)
  end

  def group_by_product(product_list) do
    Enum.reduce(product_list, [], fn item, accumulator ->
      productInAcc = Enum.find(accumulator, fn accItem -> accItem.product == item end)
      if not is_nil(productInAcc) do
        accumulator
        |> List.update_at(
            Enum.find_index(accumulator, fn accItem -> accItem === productInAcc end),
            fn accItem -> %{accItem | quantity: accItem.quantity + 1} end
          )
      else
        [%{product: item, quantity: 1} | accumulator]
      end
    end)
  end

end
