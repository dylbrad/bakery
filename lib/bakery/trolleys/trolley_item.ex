defmodule Bakery.Trolleys.TrolleyItem do
  use Ecto.Schema
  import Ecto.Changeset

  alias Bakery.Trolleys.Trolley
  alias Bakery.Products.Product
  alias Bakery.SpecialOffers.SpecialOffer

  schema "trolley_items" do
    field :quantity, :integer
    field :unit_price, :float
    belongs_to :trolley, Trolley
    belongs_to :product, Product
    belongs_to :special_offer, SpecialOffer

    timestamps()
  end

  @doc false
  def changeset(trolley_item, attrs) do
    trolley_item
    |> cast(attrs, [:quantity, :unit_price, :product_id, :special_offer_id])
    |> validate_required([:quantity, :unit_price])
  end
end
