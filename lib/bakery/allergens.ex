defmodule Bakery.Allergens do
  @moduledoc """
  The Allergens context.
  """

  import Ecto.Query, warn: false
  alias Bakery.Repo

  alias Bakery.Allergens.Allergen

  @doc """
  Returns the list of allergens.

  ## Examples

      iex> list_allergens()
      [%Allergen{}, ...]

  """
  def list_allergens do
    Repo.all(Allergen)
  end

  def get_allergens!(ids) do
    Allergen
    |> where([allergen], allergen.id in ^ids)
    |> Repo.all
  end

  @doc """
  Gets a single allergen.

  Raises `Ecto.NoResultsError` if the Allergen does not exist.

  ## Examples

      iex> get_allergen!(123)
      %Allergen{}

      iex> get_allergen!(456)
      ** (Ecto.NoResultsError)

  """
  def get_allergen!(id), do: Repo.get!(Allergen, id)

  @doc """
  Creates a allergen.

  ## Examples

      iex> create_allergen(%{field: value})
      {:ok, %Allergen{}}

      iex> create_allergen(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_allergen(attrs \\ %{}) do
    %Allergen{}
    |> Allergen.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a allergen.

  ## Examples

      iex> update_allergen(allergen, %{field: new_value})
      {:ok, %Allergen{}}

      iex> update_allergen(allergen, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_allergen(%Allergen{} = allergen, attrs) do
    allergen
    |> Allergen.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a allergen.

  ## Examples

      iex> delete_allergen(allergen)
      {:ok, %Allergen{}}

      iex> delete_allergen(allergen)
      {:error, %Ecto.Changeset{}}

  """
  def delete_allergen(%Allergen{} = allergen) do
    Repo.delete(allergen)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking allergen changes.

  ## Examples

      iex> change_allergen(allergen)
      %Ecto.Changeset{data: %Allergen{}}

  """
  def change_allergen(%Allergen{} = allergen, attrs \\ %{}) do
    Allergen.changeset(allergen, attrs)
  end
end
