defmodule Bakery.DatetimeFormat do

  def format_utc_date(datetime) do
    datetime
    |> DateTime.from_naive!("Etc/UTC")
    |> Timex.to_datetime("Europe/Dublin")
    |> Timex.format!("%d/%m/%Y", :strftime)
  end

  def format_utc_time(datetime) do
    datetime
    |> DateTime.from_naive!("Etc/UTC")
    |> Timex.to_datetime("Europe/Dublin")
    |> Timex.format!("%H:%M", :strftime)
  end

  def format_date(datetime) do
    Timex.format!(datetime, "%d/%m/%Y", :strftime)
  end

  def format_time(datetime) do
    Timex.format!(datetime, "%H:%M", :strftime)
  end
end
