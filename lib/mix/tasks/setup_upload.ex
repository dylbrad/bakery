defmodule Mix.Tasks.SetupUpload do
  use Mix.Task

  @shortdoc "Setups the upload folder"
  def run(_) do
    Mix.Task.run("app.start")

    upload_path = Application.get_env(:bakery, :upload_path)
    File.mkdir_p!(upload_path)
  end
end
