defmodule Mix.Tasks.CreateBusiness do
  use Mix.Task

  alias Bakery.Businesses

  @shortdoc "Creates a business"
  def run(_) do
    Mix.Task.run("app.start")

    name = IO.gets("Choose a name for your new business:\n")
    |> String.trim("\n")
    address1 = IO.gets("Address1:\n")
    |> String.trim("\n")
    address2 = IO.gets("Address1:\n")
    |> String.trim("\n")
    city = IO.gets("City:\n")
    |> String.trim("\n")
    county = IO.gets("County:\n")
    |> String.trim("\n")
    phone = IO.gets("Phone:\n")
    |> String.trim("\n")


    IO.inspect Businesses.create_business(%{
      "name" => name,
      "address1" => address1,
      "address2" => address2,
      "city" => city,
      "county" => county,
      "phone" => phone,
    })
  end
end
