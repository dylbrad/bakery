defmodule BakeryWeb.Router do
  use BakeryWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :authenticated do
    plug Bakery.Guardian.Pipeline
    plug Guardian.Plug.EnsureAuthenticated
  end

  scope "/", BakeryWeb do
    pipe_through :browser

    get "/", PageController, :index
    get "/images/:filename", ImageController, :download
  end

  # Other scopes may use custom stacks.
  scope "/api", BakeryWeb do
    pipe_through :api

    resources "/products", ProductController, only: [:index, :show]
    post "/orders", OrderController, :create
    get "/order_statuses", OrderStatusController, :index
    get "/recipes/by-product", RecipeController, :by_product

    post "/login",  SessionController, :login
    post "/messages", MessageController, :create
    put "/orders/:id/:confirmation_key", OrderController, :confirm
    get "/orders/:id/:confirmation_key", OrderController, :fetch_with_confirmation_key
    get "/allergens", AllergenController, :index
    get "/booking_slots", BookingSlotController, :index
    get "/booking_slots/earliest", BookingSlotController, :earliest
    get "/special_offers", SpecialOfferController, :index
    get "/settings", SettingController, :index
  end

  scope "/api/admin", BakeryWeb do
    pipe_through [:authenticated]

    resources "/customers", CustomerController, except: [:new, :edit]
    get "/orders/report", AdminOrderReportController, :report
    resources "/orders", AdminOrderController, except: [:new, :edit]
    put "/orders/:id/approve", AdminOrderController, :approve
    put "/orders/:id/decline", AdminOrderController, :decline
    resources "/products", AdminProductController, only: [:index, :create, :show, :update, :delete]
    resources "/order_statuses", OrderStatusController, only: [:index]
    resources "/messages", MessageController, only: [:index]
    resources "/images", ImageController, only: [:index, :create, :delete]
    resources "/recipes", RecipeController, only: [:index, :show, :update]
    resources "/allergens", AllergenController, only: [:index, :show, :create, :update, :delete]
    resources "/booking_slots", AdminBookingSlotController, only: [:index, :create, :delete]
    resources "/special_offers", AdminSpecialOfferController, only: [:index, :create, :delete, :show, :update]
    put "/settings", AdminSettingController, :update
    resources "/businesses", BusinessController, only: [:index, :show]
    resources "/business_users", BusinessUserController, only: [:index, :show]
  end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through :browser
      live_dashboard "/dashboard", metrics: BakeryWeb.Telemetry
    end
  end
end
