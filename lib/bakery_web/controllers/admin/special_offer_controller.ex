defmodule BakeryWeb.AdminSpecialOfferController do
  use BakeryWeb, :controller

  alias Bakery.SpecialOffers
  alias Bakery.SpecialOffers.SpecialOffer

  action_fallback BakeryWeb.FallbackController

  def index(conn, _params) do
    special_offers = SpecialOffers.list_special_offers(:all)
    render(conn, "index.json", special_offers: special_offers)
  end

  def create(conn, %{"special_offer" => special_offer_params}) do
    with {:ok, %SpecialOffer{} = special_offer} <- SpecialOffers.create_special_offer(special_offer_params) do
      special_offer = special_offer
      |> Bakery.Repo.preload([:products])

      conn
      |> put_status(:created)
      |> render("show.json", special_offer: special_offer)
    end
  end

  def show(conn, %{"id" => id}) do
    special_offer = SpecialOffers.get_special_offer!(id)
    render(conn, "show.json", special_offer: special_offer)
  end

  def update(conn, %{"id" => id, "special_offer" => special_offer_params}) do
    special_offer = SpecialOffers.get_special_offer!(id)

    with {:ok, %SpecialOffer{} = special_offer} <- SpecialOffers.update_special_offer(special_offer, special_offer_params) do
      render(conn, "show.json", special_offer: special_offer)
    end
  end

  def delete(conn, %{"id" => id}) do
    special_offer = SpecialOffers.get_special_offer!(id)

    with {:ok, %SpecialOffer{}} <- SpecialOffers.delete_special_offer(special_offer) do
      send_resp(conn, :no_content, "")
    end
  end
end
