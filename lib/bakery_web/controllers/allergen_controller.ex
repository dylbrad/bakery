defmodule BakeryWeb.AllergenController do
  use BakeryWeb, :controller

  alias Bakery.Allergens
  alias Bakery.Allergens.Allergen

  action_fallback BakeryWeb.FallbackController

  def index(conn, _params) do
    allergens = Allergens.list_allergens()
    render(conn, "index.json", allergens: allergens)
  end

  def create(conn, %{"allergen" => allergen_params}) do
    with {:ok, %Allergen{} = allergen} <- Allergens.create_allergen(allergen_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.allergen_path(conn, :show, allergen))
      |> render("show.json", allergen: allergen)
    end
  end

  def show(conn, %{"id" => id}) do
    allergen = Allergens.get_allergen!(id)
    render(conn, "show.json", allergen: allergen)
  end

  def update(conn, %{"id" => id, "allergen" => allergen_params}) do
    allergen = Allergens.get_allergen!(id)

    with {:ok, %Allergen{} = allergen} <- Allergens.update_allergen(allergen, allergen_params) do
      render(conn, "show.json", allergen: allergen)
    end
  end

  def delete(conn, %{"id" => id}) do
    allergen = Allergens.get_allergen!(id)

    with {:ok, %Allergen{}} <- Allergens.delete_allergen(allergen) do
      send_resp(conn, :no_content, "")
    end
  end
end
