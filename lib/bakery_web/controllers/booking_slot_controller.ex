defmodule BakeryWeb.BookingSlotController do
  use BakeryWeb, :controller

  alias Bakery.BookingSlots

  action_fallback BakeryWeb.FallbackController

  def index(conn, %{"date" => date}) do
    booking_slots = BookingSlots.list_booking_slots(date)
    render(conn, "index.json", booking_slots: booking_slots)
  end

  def earliest(conn, _params) do
    booking_slot = BookingSlots.get_earliest_booking_slot()
    render(conn, "show.json", booking_slot: booking_slot)
  end
end
