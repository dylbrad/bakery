defmodule BakeryWeb.TrolleyView do
  use BakeryWeb, :view
  alias BakeryWeb.TrolleyView
  alias BakeryWeb.TrolleyItemView

  def render("index.json", %{trolleys: trolleys}) do
    %{data: render_many(trolleys, TrolleyView, "trolley.json")}
  end

  def render("show.json", %{trolley: trolley}) do
    %{data: render_one(trolley, TrolleyView, "trolley.json")}
  end

  def render("trolley.json", %{trolley: trolley}) do
    %{id: trolley.id,
      delivery_price: trolley.delivery_price,
      items: render_many(trolley.items, TrolleyItemView, "trolley_item.json"),
    }
  end
end
