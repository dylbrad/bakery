defmodule BakeryWeb.MessageView do
  use BakeryWeb, :view
  alias BakeryWeb.MessageView

  def render("index.json", %{messages: messages}) do
    %{data: render_many(messages, MessageView, "message.json")}
  end

  def render("show.json", %{message: message}) do
    %{data: render_one(message, MessageView, "message.json")}
  end

  def render("message.json", %{message: message}) do
    %{
      id: message.id,
      name: message.name,
      email: message.email,
      message: message.message,
      inserted_at: message.inserted_at,
    }
  end
end
