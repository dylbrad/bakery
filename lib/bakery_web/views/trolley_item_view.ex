defmodule BakeryWeb.TrolleyItemView do
  use BakeryWeb, :view
  alias BakeryWeb.TrolleyItemView

  def render("index.json", %{trolley_items: trolley_items}) do
    %{data: render_many(trolley_items, TrolleyItemView, "trolley_item.json")}
  end

  def render("show.json", %{trolley_item: trolley_item}) do
    %{data: render_one(trolley_item, TrolleyItemView, "trolley_item.json")}
  end

  def render("trolley_item.json", %{trolley_item: trolley_item}) do
    %{id: trolley_item.id,
      quantity: trolley_item.quantity,
      unit_price: trolley_item.unit_price,
      product_id: trolley_item.product_id,
      special_offer_id: trolley_item.special_offer_id,
    }
  end
end
