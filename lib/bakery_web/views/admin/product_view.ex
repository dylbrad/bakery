defmodule BakeryWeb.AdminProductView do
  use BakeryWeb, :view
  # alias BakeryWeb.AdminProductView

  def render("index.json", %{products: products}) do
    %{data: render_many(products, BakeryWeb.ProductView, "product.json")}
  end

  def render("show.json", %{product: product}) do
    %{data: render_one(product, BakeryWeb.ProductView, "product.json")}
  end

  def render("product.json", %{product: product}) do
    %{id: product.id,
      name: product.name,
      key: product.key,
      description: product.description,
      image: product.image,
      price: product.price,
      disabled: product.disabled,
      order: product.order,
    }
  end
end
