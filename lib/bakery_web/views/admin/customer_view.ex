defmodule BakeryWeb.AdminCustomerView do
  use BakeryWeb, :view
  alias BakeryWeb.AdminCustomerView

  def render("index.json", %{customers: customers}) do
    %{data: render_many(customers, AdminCustomerView, "customer.json")}
  end

  def render("show.json", %{customer: customer}) do
    %{data: render_one(customer, AdminCustomerView, "customer.json")}
  end

  def render("customer.json", %{admin_customer: customer}) do
    %{id: customer.id,
      first_name: customer.first_name,
      last_name: customer.last_name,
      email: customer.email,
      phone: customer.phone,
      address1: customer.address1,
      address2: customer.address2,
      city: customer.city,
      county: customer.county}
  end
end
