defmodule BakeryWeb.AdminOrderReportView do
  use BakeryWeb, :view

  alias Elixlsx.{Workbook, Sheet}

  @header [
    "Customer",
    "Delivery Date",
    "Basket",
    "Total (including delivery)",
    "Is Delivery?",
    "Delivery price",
    "Payment Type",
    "Transaction Code",
  ]

  def render("report.xlsx", %{rows: rows}) do
    report_generator(rows)
    |> Elixlsx.write_to_memory("report.xlsx")
    |> elem(1)
    |> elem(1)
  end

  def report_generator(rows) do
    %Workbook{sheets: [%Sheet{name: "Report", rows: [@header] ++ rows}]}
  end
end
