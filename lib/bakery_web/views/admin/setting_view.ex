defmodule BakeryWeb.AdminSettingView do
  use BakeryWeb, :view
  alias BakeryWeb.AdminSettingView

  def render("index.json", %{settings: settings}) do
    %{data: render_many(settings, AdminSettingView, "setting.json")}
  end

  def render("show.json", %{setting: setting}) do
    %{data: render_one(setting, AdminSettingView, "setting.json")}
  end

  def render("setting.json", %{admin_setting: setting}) do
    %{id: setting.id,
      name: setting.name,
      value: setting.value,
      type: setting.type}
  end
end
