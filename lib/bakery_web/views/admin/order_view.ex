defmodule BakeryWeb.AdminOrderView do
  use BakeryWeb, :view
  alias BakeryWeb.AdminOrderView
  alias BakeryWeb.DeliveryView
  alias BakeryWeb.TrolleyView
  alias BakeryWeb.AdminPaymentView
  alias BakeryWeb.AdminCustomerView

  def render("index.json", %{orders: orders}) do
    %{data: render_many(orders, AdminOrderView, "order.json")}
  end

  def render("show.json", %{order: order}) do
    %{data: render_one(order, AdminOrderView, "order.json")}
  end

  def render("order.json", %{admin_order: order}) do
    %{
      id: order.id,
      delivery: render_one(order.delivery, DeliveryView, "delivery.json"),
      trolley: render_one(order.trolley, TrolleyView, "trolley.json"),
      payment: render_one(order.payment, AdminPaymentView, "payment.json"),
      customer: render_one(order.customer, AdminCustomerView, "customer.json"),
      status_id: order.status_id,
      inserted_at: order.inserted_at,
      is_delivery: order.is_delivery,
    }
  end
end
