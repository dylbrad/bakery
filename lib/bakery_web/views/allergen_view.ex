defmodule BakeryWeb.AllergenView do
  use BakeryWeb, :view
  alias BakeryWeb.AllergenView

  def render("index.json", %{allergens: allergens}) do
    %{data: render_many(allergens, AllergenView, "allergen.json")}
  end

  def render("show.json", %{allergen: allergen}) do
    %{data: render_one(allergen, AllergenView, "allergen.json")}
  end

  def render("allergen.json", %{allergen: allergen}) do
    %{id: allergen.id,
      name: allergen.name,
      image: allergen.image}
  end
end
