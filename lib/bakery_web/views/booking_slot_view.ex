defmodule BakeryWeb.BookingSlotView do
  use BakeryWeb, :view
  alias BakeryWeb.BookingSlotView

  def render("index.json", %{booking_slots: booking_slots}) do
    %{data: render_many(booking_slots, BookingSlotView, "booking_slot.json")}
  end

  def render("show.json", %{booking_slot: booking_slot}) do
    %{data: render_one(booking_slot, BookingSlotView, "booking_slot.json")}
  end

  def render("booking_slot.json", %{booking_slot: booking_slot}) do
    %{id: booking_slot.id,
      date: booking_slot.date,
      time_slot: booking_slot.time_slot,
      is_delivery: booking_slot.is_delivery,
    }
  end
end
