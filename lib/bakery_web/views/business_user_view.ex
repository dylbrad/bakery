defmodule BakeryWeb.BusinessUserView do
  use BakeryWeb, :view
  alias BakeryWeb.BusinessUserView

  def render("index.json", %{business_users: business_users}) do
    %{data: render_many(business_users, BusinessUserView, "business_user.json")}
  end

  def render("show.json", %{business_user: business_user}) do
    %{data: render_one(business_user, BusinessUserView, "business_user.json")}
  end

  def render("business_user.json", %{business_user: business_user}) do
    %{id: business_user.id,
      email: business_user.email,
      hashed_password: business_user.hashed_password}
  end
end
