defmodule BakeryWeb.BusinessView do
  use BakeryWeb, :view
  alias BakeryWeb.BusinessView

  def render("index.json", %{businesses: businesses}) do
    %{data: render_many(businesses, BusinessView, "business.json")}
  end

  def render("show.json", %{business: business}) do
    %{data: render_one(business, BusinessView, "business.json")}
  end

  def render("business.json", %{business: business}) do
    %{id: business.id,
      name: business.name,
      address1: business.address1,
      address2: business.address2,
      city: business.city,
      county: business.county,
      phone: business.phone}
  end
end
