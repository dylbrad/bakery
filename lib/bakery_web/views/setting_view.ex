defmodule BakeryWeb.SettingView do
  use BakeryWeb, :view
  alias BakeryWeb.SettingView

  def render("index.json", %{settings: settings}) do
    %{data: render_many(settings, SettingView, "setting.json")}
  end

  def render("show.json", %{setting: setting}) do
    %{data: render_one(setting, SettingView, "setting.json")}
  end

  def render("setting.json", %{setting: setting}) do
    %{id: setting.id,
      name: setting.name,
      value: setting.value,
      type: setting.type}
  end
end
