defmodule Bakery.AllergensTest do
  use Bakery.DataCase

  alias Bakery.Allergens

  describe "allergens" do
    alias Bakery.Allergens.Allergen

    @valid_attrs %{image: "some image", name: "some name"}
    @update_attrs %{image: "some updated image", name: "some updated name"}
    @invalid_attrs %{image: nil, name: nil}

    def allergen_fixture(attrs \\ %{}) do
      {:ok, allergen} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Allergens.create_allergen()

      allergen
    end

    test "list_allergens/0 returns all allergens" do
      allergen = allergen_fixture()
      assert Allergens.list_allergens() == [allergen]
    end

    test "get_allergen!/1 returns the allergen with given id" do
      allergen = allergen_fixture()
      assert Allergens.get_allergen!(allergen.id) == allergen
    end

    test "create_allergen/1 with valid data creates a allergen" do
      assert {:ok, %Allergen{} = allergen} = Allergens.create_allergen(@valid_attrs)
      assert allergen.image == "some image"
      assert allergen.name == "some name"
    end

    test "create_allergen/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Allergens.create_allergen(@invalid_attrs)
    end

    test "update_allergen/2 with valid data updates the allergen" do
      allergen = allergen_fixture()
      assert {:ok, %Allergen{} = allergen} = Allergens.update_allergen(allergen, @update_attrs)
      assert allergen.image == "some updated image"
      assert allergen.name == "some updated name"
    end

    test "update_allergen/2 with invalid data returns error changeset" do
      allergen = allergen_fixture()
      assert {:error, %Ecto.Changeset{}} = Allergens.update_allergen(allergen, @invalid_attrs)
      assert allergen == Allergens.get_allergen!(allergen.id)
    end

    test "delete_allergen/1 deletes the allergen" do
      allergen = allergen_fixture()
      assert {:ok, %Allergen{}} = Allergens.delete_allergen(allergen)
      assert_raise Ecto.NoResultsError, fn -> Allergens.get_allergen!(allergen.id) end
    end

    test "change_allergen/1 returns a allergen changeset" do
      allergen = allergen_fixture()
      assert %Ecto.Changeset{} = Allergens.change_allergen(allergen)
    end
  end
end
