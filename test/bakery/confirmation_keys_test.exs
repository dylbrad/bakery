defmodule Bakery.ConfirmationKeysTest do
  use Bakery.DataCase

  alias Bakery.ConfirmationKeys

  describe "confirmation_keys" do
    alias Bakery.ConfirmationKeys.ConfirmationKey

    @valid_attrs %{confirmation_key: "some confirmation_key", expires_on: "2010-04-17T14:00:00Z"}
    @update_attrs %{confirmation_key: "some updated confirmation_key", expires_on: "2011-05-18T15:01:01Z"}
    @invalid_attrs %{confirmation_key: nil, expires_on: nil}

    def confirmation_key_fixture(attrs \\ %{}) do
      {:ok, confirmation_key} =
        attrs
        |> Enum.into(@valid_attrs)
        |> ConfirmationKeys.create_confirmation_key()

      confirmation_key
    end

    test "list_confirmation_keys/0 returns all confirmation_keys" do
      confirmation_key = confirmation_key_fixture()
      assert ConfirmationKeys.list_confirmation_keys() == [confirmation_key]
    end

    test "get_confirmation_key!/1 returns the confirmation_key with given id" do
      confirmation_key = confirmation_key_fixture()
      assert ConfirmationKeys.get_confirmation_key!(confirmation_key.id) == confirmation_key
    end

    test "create_confirmation_key/1 with valid data creates a confirmation_key" do
      assert {:ok, %ConfirmationKey{} = confirmation_key} = ConfirmationKeys.create_confirmation_key(@valid_attrs)
      assert confirmation_key.confirmation_key == "some confirmation_key"
      assert confirmation_key.expires_on == DateTime.from_naive!(~N[2010-04-17T14:00:00Z], "Etc/UTC")
    end

    test "create_confirmation_key/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = ConfirmationKeys.create_confirmation_key(@invalid_attrs)
    end

    test "update_confirmation_key/2 with valid data updates the confirmation_key" do
      confirmation_key = confirmation_key_fixture()
      assert {:ok, %ConfirmationKey{} = confirmation_key} = ConfirmationKeys.update_confirmation_key(confirmation_key, @update_attrs)
      assert confirmation_key.confirmation_key == "some updated confirmation_key"
      assert confirmation_key.expires_on == DateTime.from_naive!(~N[2011-05-18T15:01:01Z], "Etc/UTC")
    end

    test "update_confirmation_key/2 with invalid data returns error changeset" do
      confirmation_key = confirmation_key_fixture()
      assert {:error, %Ecto.Changeset{}} = ConfirmationKeys.update_confirmation_key(confirmation_key, @invalid_attrs)
      assert confirmation_key == ConfirmationKeys.get_confirmation_key!(confirmation_key.id)
    end

    test "delete_confirmation_key/1 deletes the confirmation_key" do
      confirmation_key = confirmation_key_fixture()
      assert {:ok, %ConfirmationKey{}} = ConfirmationKeys.delete_confirmation_key(confirmation_key)
      assert_raise Ecto.NoResultsError, fn -> ConfirmationKeys.get_confirmation_key!(confirmation_key.id) end
    end

    test "change_confirmation_key/1 returns a confirmation_key changeset" do
      confirmation_key = confirmation_key_fixture()
      assert %Ecto.Changeset{} = ConfirmationKeys.change_confirmation_key(confirmation_key)
    end
  end
end
