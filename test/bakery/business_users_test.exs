defmodule Bakery.BusinessUsersTest do
  use Bakery.DataCase

  alias Bakery.BusinessUsers

  describe "business_users" do
    alias Bakery.BusinessUsers.BusinessUser

    @valid_attrs %{email: "some email", hashed_password: "some hashed_password"}
    @update_attrs %{email: "some updated email", hashed_password: "some updated hashed_password"}
    @invalid_attrs %{email: nil, hashed_password: nil}

    def business_user_fixture(attrs \\ %{}) do
      {:ok, business_user} =
        attrs
        |> Enum.into(@valid_attrs)
        |> BusinessUsers.create_business_user()

      business_user
    end

    test "list_business_users/0 returns all business_users" do
      business_user = business_user_fixture()
      assert BusinessUsers.list_business_users() == [business_user]
    end

    test "get_business_user!/1 returns the business_user with given id" do
      business_user = business_user_fixture()
      assert BusinessUsers.get_business_user!(business_user.id) == business_user
    end

    test "create_business_user/1 with valid data creates a business_user" do
      assert {:ok, %BusinessUser{} = business_user} = BusinessUsers.create_business_user(@valid_attrs)
      assert business_user.email == "some email"
      assert business_user.hashed_password == "some hashed_password"
    end

    test "create_business_user/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = BusinessUsers.create_business_user(@invalid_attrs)
    end

    test "update_business_user/2 with valid data updates the business_user" do
      business_user = business_user_fixture()
      assert {:ok, %BusinessUser{} = business_user} = BusinessUsers.update_business_user(business_user, @update_attrs)
      assert business_user.email == "some updated email"
      assert business_user.hashed_password == "some updated hashed_password"
    end

    test "update_business_user/2 with invalid data returns error changeset" do
      business_user = business_user_fixture()
      assert {:error, %Ecto.Changeset{}} = BusinessUsers.update_business_user(business_user, @invalid_attrs)
      assert business_user == BusinessUsers.get_business_user!(business_user.id)
    end

    test "delete_business_user/1 deletes the business_user" do
      business_user = business_user_fixture()
      assert {:ok, %BusinessUser{}} = BusinessUsers.delete_business_user(business_user)
      assert_raise Ecto.NoResultsError, fn -> BusinessUsers.get_business_user!(business_user.id) end
    end

    test "change_business_user/1 returns a business_user changeset" do
      business_user = business_user_fixture()
      assert %Ecto.Changeset{} = BusinessUsers.change_business_user(business_user)
    end
  end
end
