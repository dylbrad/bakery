defmodule Bakery.DeliveriesTest do
  use Bakery.DataCase

  alias Bakery.Deliveries

  describe "deliveries" do
    alias Bakery.Deliveries.Delivery

    @valid_attrs %{address1: "some address1", address2: "some address2", city: "some city", county: "some county", delivery_date: "2010-04-17T14:00:00Z", email: "some email", first_name: "some first_name", last_name: "some last_name", message: "some message", phone: "some phone"}
    @update_attrs %{address1: "some updated address1", address2: "some updated address2", city: "some updated city", county: "some updated county", delivery_date: "2011-05-18T15:01:01Z", email: "some updated email", first_name: "some updated first_name", last_name: "some updated last_name", message: "some updated message", phone: "some updated phone"}
    @invalid_attrs %{address1: nil, address2: nil, city: nil, county: nil, delivery_date: nil, email: nil, first_name: nil, last_name: nil, message: nil, phone: nil}

    def delivery_fixture(attrs \\ %{}) do
      {:ok, delivery} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Deliveries.create_delivery()

      delivery
    end

    test "list_deliveries/0 returns all deliveries" do
      delivery = delivery_fixture()
      assert Deliveries.list_deliveries() == [delivery]
    end

    test "get_delivery!/1 returns the delivery with given id" do
      delivery = delivery_fixture()
      assert Deliveries.get_delivery!(delivery.id) == delivery
    end

    test "create_delivery/1 with valid data creates a delivery" do
      assert {:ok, %Delivery{} = delivery} = Deliveries.create_delivery(@valid_attrs)
      assert delivery.address1 == "some address1"
      assert delivery.address2 == "some address2"
      assert delivery.city == "some city"
      assert delivery.county == "some county"
      assert delivery.delivery_date == DateTime.from_naive!(~N[2010-04-17T14:00:00Z], "Etc/UTC")
      assert delivery.email == "some email"
      assert delivery.first_name == "some first_name"
      assert delivery.last_name == "some last_name"
      assert delivery.message == "some message"
      assert delivery.phone == "some phone"
    end

    test "create_delivery/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Deliveries.create_delivery(@invalid_attrs)
    end

    test "update_delivery/2 with valid data updates the delivery" do
      delivery = delivery_fixture()
      assert {:ok, %Delivery{} = delivery} = Deliveries.update_delivery(delivery, @update_attrs)
      assert delivery.address1 == "some updated address1"
      assert delivery.address2 == "some updated address2"
      assert delivery.city == "some updated city"
      assert delivery.county == "some updated county"
      assert delivery.delivery_date == DateTime.from_naive!(~N[2011-05-18T15:01:01Z], "Etc/UTC")
      assert delivery.email == "some updated email"
      assert delivery.first_name == "some updated first_name"
      assert delivery.last_name == "some updated last_name"
      assert delivery.message == "some updated message"
      assert delivery.phone == "some updated phone"
    end

    test "update_delivery/2 with invalid data returns error changeset" do
      delivery = delivery_fixture()
      assert {:error, %Ecto.Changeset{}} = Deliveries.update_delivery(delivery, @invalid_attrs)
      assert delivery == Deliveries.get_delivery!(delivery.id)
    end

    test "delete_delivery/1 deletes the delivery" do
      delivery = delivery_fixture()
      assert {:ok, %Delivery{}} = Deliveries.delete_delivery(delivery)
      assert_raise Ecto.NoResultsError, fn -> Deliveries.get_delivery!(delivery.id) end
    end

    test "change_delivery/1 returns a delivery changeset" do
      delivery = delivery_fixture()
      assert %Ecto.Changeset{} = Deliveries.change_delivery(delivery)
    end
  end
end
