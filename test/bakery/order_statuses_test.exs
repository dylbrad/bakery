defmodule Bakery.OrderStatusesTest do
  use Bakery.DataCase

  alias Bakery.OrderStatuses

  describe "order_statuses" do
    alias Bakery.OrderStatuses.OrderStatus

    @valid_attrs %{status: "some status"}
    @update_attrs %{status: "some updated status"}
    @invalid_attrs %{status: nil}

    def order_status_fixture(attrs \\ %{}) do
      {:ok, order_status} =
        attrs
        |> Enum.into(@valid_attrs)
        |> OrderStatuses.create_order_status()

      order_status
    end

    test "list_order_statuses/0 returns all order_statuses" do
      order_status = order_status_fixture()
      assert OrderStatuses.list_order_statuses() == [order_status]
    end

    test "get_order_status!/1 returns the order_status with given id" do
      order_status = order_status_fixture()
      assert OrderStatuses.get_order_status!(order_status.id) == order_status
    end

    test "create_order_status/1 with valid data creates a order_status" do
      assert {:ok, %OrderStatus{} = order_status} = OrderStatuses.create_order_status(@valid_attrs)
      assert order_status.status == "some status"
    end

    test "create_order_status/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = OrderStatuses.create_order_status(@invalid_attrs)
    end

    test "update_order_status/2 with valid data updates the order_status" do
      order_status = order_status_fixture()
      assert {:ok, %OrderStatus{} = order_status} = OrderStatuses.update_order_status(order_status, @update_attrs)
      assert order_status.status == "some updated status"
    end

    test "update_order_status/2 with invalid data returns error changeset" do
      order_status = order_status_fixture()
      assert {:error, %Ecto.Changeset{}} = OrderStatuses.update_order_status(order_status, @invalid_attrs)
      assert order_status == OrderStatuses.get_order_status!(order_status.id)
    end

    test "delete_order_status/1 deletes the order_status" do
      order_status = order_status_fixture()
      assert {:ok, %OrderStatus{}} = OrderStatuses.delete_order_status(order_status)
      assert_raise Ecto.NoResultsError, fn -> OrderStatuses.get_order_status!(order_status.id) end
    end

    test "change_order_status/1 returns a order_status changeset" do
      order_status = order_status_fixture()
      assert %Ecto.Changeset{} = OrderStatuses.change_order_status(order_status)
    end
  end
end
