defmodule Bakery.TrolleysTest do
  use Bakery.DataCase

  alias Bakery.Trolleys

  describe "trolleys" do
    alias Bakery.Trolleys.Trolley

    @valid_attrs %{delivery_price: 120.5}
    @update_attrs %{delivery_price: 456.7}
    @invalid_attrs %{delivery_price: nil}

    def trolley_fixture(attrs \\ %{}) do
      {:ok, trolley} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Trolleys.create_trolley()

      trolley
    end

    test "list_trolleys/0 returns all trolleys" do
      trolley = trolley_fixture()
      assert Trolleys.list_trolleys() == [trolley]
    end

    test "get_trolley!/1 returns the trolley with given id" do
      trolley = trolley_fixture()
      assert Trolleys.get_trolley!(trolley.id) == trolley
    end

    test "create_trolley/1 with valid data creates a trolley" do
      assert {:ok, %Trolley{} = trolley} = Trolleys.create_trolley(@valid_attrs)
      assert trolley.delivery_price == 120.5
    end

    test "create_trolley/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Trolleys.create_trolley(@invalid_attrs)
    end

    test "update_trolley/2 with valid data updates the trolley" do
      trolley = trolley_fixture()
      assert {:ok, %Trolley{} = trolley} = Trolleys.update_trolley(trolley, @update_attrs)
      assert trolley.delivery_price == 456.7
    end

    test "update_trolley/2 with invalid data returns error changeset" do
      trolley = trolley_fixture()
      assert {:error, %Ecto.Changeset{}} = Trolleys.update_trolley(trolley, @invalid_attrs)
      assert trolley == Trolleys.get_trolley!(trolley.id)
    end

    test "delete_trolley/1 deletes the trolley" do
      trolley = trolley_fixture()
      assert {:ok, %Trolley{}} = Trolleys.delete_trolley(trolley)
      assert_raise Ecto.NoResultsError, fn -> Trolleys.get_trolley!(trolley.id) end
    end

    test "change_trolley/1 returns a trolley changeset" do
      trolley = trolley_fixture()
      assert %Ecto.Changeset{} = Trolleys.change_trolley(trolley)
    end
  end

  describe "trolley_items" do
    alias Bakery.Trolleys.TrolleyItem

    @valid_attrs %{quantity: 42, unit_price: 120.5}
    @update_attrs %{quantity: 43, unit_price: 456.7}
    @invalid_attrs %{quantity: nil, unit_price: nil}

    def trolley_item_fixture(attrs \\ %{}) do
      {:ok, trolley_item} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Trolleys.create_trolley_item()

      trolley_item
    end

    test "list_trolley_items/0 returns all trolley_items" do
      trolley_item = trolley_item_fixture()
      assert Trolleys.list_trolley_items() == [trolley_item]
    end

    test "get_trolley_item!/1 returns the trolley_item with given id" do
      trolley_item = trolley_item_fixture()
      assert Trolleys.get_trolley_item!(trolley_item.id) == trolley_item
    end

    test "create_trolley_item/1 with valid data creates a trolley_item" do
      assert {:ok, %TrolleyItem{} = trolley_item} = Trolleys.create_trolley_item(@valid_attrs)
      assert trolley_item.quantity == 42
      assert trolley_item.unit_price == 120.5
    end

    test "create_trolley_item/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Trolleys.create_trolley_item(@invalid_attrs)
    end

    test "update_trolley_item/2 with valid data updates the trolley_item" do
      trolley_item = trolley_item_fixture()
      assert {:ok, %TrolleyItem{} = trolley_item} = Trolleys.update_trolley_item(trolley_item, @update_attrs)
      assert trolley_item.quantity == 43
      assert trolley_item.unit_price == 456.7
    end

    test "update_trolley_item/2 with invalid data returns error changeset" do
      trolley_item = trolley_item_fixture()
      assert {:error, %Ecto.Changeset{}} = Trolleys.update_trolley_item(trolley_item, @invalid_attrs)
      assert trolley_item == Trolleys.get_trolley_item!(trolley_item.id)
    end

    test "delete_trolley_item/1 deletes the trolley_item" do
      trolley_item = trolley_item_fixture()
      assert {:ok, %TrolleyItem{}} = Trolleys.delete_trolley_item(trolley_item)
      assert_raise Ecto.NoResultsError, fn -> Trolleys.get_trolley_item!(trolley_item.id) end
    end

    test "change_trolley_item/1 returns a trolley_item changeset" do
      trolley_item = trolley_item_fixture()
      assert %Ecto.Changeset{} = Trolleys.change_trolley_item(trolley_item)
    end
  end
end
