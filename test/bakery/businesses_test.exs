defmodule Bakery.BusinessesTest do
  use Bakery.DataCase

  alias Bakery.Businesses

  describe "businesses" do
    alias Bakery.Businesses.Business

    @valid_attrs %{address1: "some address1", address2: "some address2", city: "some city", county: "some county", name: "some name", phone: "some phone"}
    @update_attrs %{address1: "some updated address1", address2: "some updated address2", city: "some updated city", county: "some updated county", name: "some updated name", phone: "some updated phone"}
    @invalid_attrs %{address1: nil, address2: nil, city: nil, county: nil, name: nil, phone: nil}

    def business_fixture(attrs \\ %{}) do
      {:ok, business} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Businesses.create_business()

      business
    end

    test "list_businesses/0 returns all businesses" do
      business = business_fixture()
      assert Businesses.list_businesses() == [business]
    end

    test "get_business!/1 returns the business with given id" do
      business = business_fixture()
      assert Businesses.get_business!(business.id) == business
    end

    test "create_business/1 with valid data creates a business" do
      assert {:ok, %Business{} = business} = Businesses.create_business(@valid_attrs)
      assert business.address1 == "some address1"
      assert business.address2 == "some address2"
      assert business.city == "some city"
      assert business.county == "some county"
      assert business.name == "some name"
      assert business.phone == "some phone"
    end

    test "create_business/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Businesses.create_business(@invalid_attrs)
    end

    test "update_business/2 with valid data updates the business" do
      business = business_fixture()
      assert {:ok, %Business{} = business} = Businesses.update_business(business, @update_attrs)
      assert business.address1 == "some updated address1"
      assert business.address2 == "some updated address2"
      assert business.city == "some updated city"
      assert business.county == "some updated county"
      assert business.name == "some updated name"
      assert business.phone == "some updated phone"
    end

    test "update_business/2 with invalid data returns error changeset" do
      business = business_fixture()
      assert {:error, %Ecto.Changeset{}} = Businesses.update_business(business, @invalid_attrs)
      assert business == Businesses.get_business!(business.id)
    end

    test "delete_business/1 deletes the business" do
      business = business_fixture()
      assert {:ok, %Business{}} = Businesses.delete_business(business)
      assert_raise Ecto.NoResultsError, fn -> Businesses.get_business!(business.id) end
    end

    test "change_business/1 returns a business changeset" do
      business = business_fixture()
      assert %Ecto.Changeset{} = Businesses.change_business(business)
    end
  end
end
