defmodule Bakery.CustomersTest do
  use Bakery.DataCase

  alias Bakery.Customers

  describe "customers" do
    alias Bakery.Customers.Customer

    @valid_attrs %{address1: "some address1", address2: "some address2", city: "some city", county: "some county", email: "some email", first_name: "some first_name", last_name: "some last_name", phone: "some phone"}
    @update_attrs %{address1: "some updated address1", address2: "some updated address2", city: "some updated city", county: "some updated county", email: "some updated email", first_name: "some updated first_name", last_name: "some updated last_name", phone: "some updated phone"}
    @invalid_attrs %{address1: nil, address2: nil, city: nil, county: nil, email: nil, first_name: nil, last_name: nil, phone: nil}

    def customer_fixture(attrs \\ %{}) do
      {:ok, customer} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Customers.create_customer()

      customer
    end

    test "list_customers/0 returns all customers" do
      customer = customer_fixture()
      assert Customers.list_customers() == [customer]
    end

    test "get_customer!/1 returns the customer with given id" do
      customer = customer_fixture()
      assert Customers.get_customer!(customer.id) == customer
    end

    test "create_customer/1 with valid data creates a customer" do
      assert {:ok, %Customer{} = customer} = Customers.create_customer(@valid_attrs)
      assert customer.address1 == "some address1"
      assert customer.address2 == "some address2"
      assert customer.city == "some city"
      assert customer.county == "some county"
      assert customer.email == "some email"
      assert customer.first_name == "some first_name"
      assert customer.last_name == "some last_name"
      assert customer.phone == "some phone"
    end

    test "create_customer/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Customers.create_customer(@invalid_attrs)
    end

    test "update_customer/2 with valid data updates the customer" do
      customer = customer_fixture()
      assert {:ok, %Customer{} = customer} = Customers.update_customer(customer, @update_attrs)
      assert customer.address1 == "some updated address1"
      assert customer.address2 == "some updated address2"
      assert customer.city == "some updated city"
      assert customer.county == "some updated county"
      assert customer.email == "some updated email"
      assert customer.first_name == "some updated first_name"
      assert customer.last_name == "some updated last_name"
      assert customer.phone == "some updated phone"
    end

    test "update_customer/2 with invalid data returns error changeset" do
      customer = customer_fixture()
      assert {:error, %Ecto.Changeset{}} = Customers.update_customer(customer, @invalid_attrs)
      assert customer == Customers.get_customer!(customer.id)
    end

    test "delete_customer/1 deletes the customer" do
      customer = customer_fixture()
      assert {:ok, %Customer{}} = Customers.delete_customer(customer)
      assert_raise Ecto.NoResultsError, fn -> Customers.get_customer!(customer.id) end
    end

    test "change_customer/1 returns a customer changeset" do
      customer = customer_fixture()
      assert %Ecto.Changeset{} = Customers.change_customer(customer)
    end
  end
end
