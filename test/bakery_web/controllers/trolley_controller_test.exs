defmodule BakeryWeb.TrolleyControllerTest do
  use BakeryWeb.ConnCase

  alias Bakery.Trolleys
  alias Bakery.Trolleys.Trolley

  @create_attrs %{
    delivery_price: 120.5,
    items: [%{quantity: 2, unit_price: 33.2}],
  }
  @update_attrs %{
    delivery_price: 456.7,
    items: [],
  }
  @invalid_attrs %{delivery_price: nil}

  def fixture(:trolley) do
    {:ok, trolley} = Trolleys.create_trolley(@create_attrs)
    trolley
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all trolleys", %{conn: conn} do
      conn = get(conn, Routes.trolley_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create trolley" do
    test "renders trolley when data is valid", %{conn: conn} do
      conn = post(conn, Routes.trolley_path(conn, :create), trolley: @create_attrs)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, Routes.trolley_path(conn, :show, id))

      assert %{
               "id" => id,
               "delivery_price" => 120.5
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.trolley_path(conn, :create), trolley: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update trolley" do
    setup [:create_trolley]

    test "renders trolley when data is valid", %{conn: conn, trolley: %Trolley{id: id} = trolley} do
      conn = put(conn, Routes.trolley_path(conn, :update, trolley), trolley: @update_attrs)
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.trolley_path(conn, :show, id))

      assert %{
               "id" => id,
               "delivery_price" => 456.7
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, trolley: trolley} do
      conn = put(conn, Routes.trolley_path(conn, :update, trolley), trolley: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete trolley" do
    setup [:create_trolley]

    test "deletes chosen trolley", %{conn: conn, trolley: trolley} do
      conn = delete(conn, Routes.trolley_path(conn, :delete, trolley))
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, Routes.trolley_path(conn, :show, trolley))
      end
    end
  end

  defp create_trolley(_) do
    trolley = fixture(:trolley)
    %{trolley: trolley}
  end
end
