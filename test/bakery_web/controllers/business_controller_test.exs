defmodule BakeryWeb.BusinessControllerTest do
  use BakeryWeb.ConnCase

  alias Bakery.Businesses
  alias Bakery.Businesses.Business

  @create_attrs %{
    address1: "some address1",
    address2: "some address2",
    city: "some city",
    county: "some county",
    name: "some name",
    phone: "some phone"
  }
  @update_attrs %{
    address1: "some updated address1",
    address2: "some updated address2",
    city: "some updated city",
    county: "some updated county",
    name: "some updated name",
    phone: "some updated phone"
  }
  @invalid_attrs %{address1: nil, address2: nil, city: nil, county: nil, name: nil, phone: nil}

  def fixture(:business) do
    {:ok, business} = Businesses.create_business(@create_attrs)
    business
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all businesses", %{conn: conn} do
      conn = get(conn, Routes.business_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create business" do
    test "renders business when data is valid", %{conn: conn} do
      conn = post(conn, Routes.business_path(conn, :create), business: @create_attrs)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, Routes.business_path(conn, :show, id))

      assert %{
               "id" => id,
               "address1" => "some address1",
               "address2" => "some address2",
               "city" => "some city",
               "county" => "some county",
               "name" => "some name",
               "phone" => "some phone"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.business_path(conn, :create), business: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update business" do
    setup [:create_business]

    test "renders business when data is valid", %{conn: conn, business: %Business{id: id} = business} do
      conn = put(conn, Routes.business_path(conn, :update, business), business: @update_attrs)
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.business_path(conn, :show, id))

      assert %{
               "id" => id,
               "address1" => "some updated address1",
               "address2" => "some updated address2",
               "city" => "some updated city",
               "county" => "some updated county",
               "name" => "some updated name",
               "phone" => "some updated phone"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, business: business} do
      conn = put(conn, Routes.business_path(conn, :update, business), business: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete business" do
    setup [:create_business]

    test "deletes chosen business", %{conn: conn, business: business} do
      conn = delete(conn, Routes.business_path(conn, :delete, business))
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, Routes.business_path(conn, :show, business))
      end
    end
  end

  defp create_business(_) do
    business = fixture(:business)
    %{business: business}
  end
end
