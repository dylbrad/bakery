defmodule BakeryWeb.OrderControllerTest do
  use BakeryWeb.ConnCase

  alias Bakery.Orders
  alias Bakery.Orders.Order
  alias Bakery.Products

  @products_attrs [
    %{
      description: "some description",
      image: "some image",
      key: "baguette",
      name: "Baguette",
      price: 2.3,
    },
    %{
      description: "some description",
      image: "some image",
      key: "croissant",
      name: "Croissant",
      price: 4.3,
    },
  ]

  @create_attrs %{
    delivery: %{
      address1: "some address1",
      address2: "some address2",
      city: "some city",
      county: "some county",
      delivery_date: "2010-04-17T14:00:00Z",
      email: "some email",
      first_name: "some first_name",
      last_name: "some last_name",
      message: "some message",
      phone: "some phone"
    },
    trolley: %{
      delivery_price: 3.2,
      items: [
        %{quantity: 2, unit_price: 33.2},
      ],
    },
  }
  @update_attrs %{

  }
  @invalid_attrs %{}

  def fixture(:order) do
    {:ok, order} = Orders.create_order(@create_attrs)
    order
  end

  setup %{conn: conn} do
    @products_attrs
    |> Enum.each(fn product -> Products.create_product!(product) end)
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all orders", %{conn: conn} do
      conn = get(conn, Routes.order_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create order" do
    test "renders order when data is valid", %{conn: conn} do
      product = hd(Products.list_products()).id
      attrs = %{
        @create_attrs | trolley: %{
          @create_attrs.trolley | items: Enum.map(
            @create_attrs.trolley.items,
            fn item -> Map.put(item, :product_id, product) end
          )
        }
      }

      conn = post(conn, Routes.order_path(conn, :create), order: attrs)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, Routes.order_path(conn, :show, id))

      assert %{
               "id" => id
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.order_path(conn, :create), order: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update order" do
    setup [:create_order]

    test "renders order when data is valid", %{conn: conn, order: %Order{id: id} = order} do
      conn = put(conn, Routes.order_path(conn, :update, order), order: @update_attrs)
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.order_path(conn, :show, id))

      assert %{
               "id" => id
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, order: order} do
      conn = put(conn, Routes.order_path(conn, :update, order), order: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete order" do
    setup [:create_order]

    test "deletes chosen order", %{conn: conn, order: order} do
      conn = delete(conn, Routes.order_path(conn, :delete, order))
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, Routes.order_path(conn, :show, order))
      end
    end
  end

  defp create_order(_) do
    order = fixture(:order)
    %{order: order}
  end
end
