defmodule BakeryWeb.OrderStatusControllerTest do
  use BakeryWeb.ConnCase

  alias Bakery.OrderStatuses
  alias Bakery.OrderStatuses.OrderStatus

  def fixture(:order_status) do
    {:ok, order_status} = OrderStatuses.create_order_status(@create_attrs)
    order_status
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all order_statuses", %{conn: conn} do
      conn = get(conn, Routes.order_status_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end
end
