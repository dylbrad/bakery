defmodule BakeryWeb.BookingSlotControllerTest do
  use BakeryWeb.ConnCase

  alias Bakery.BookingSlots
  alias Bakery.BookingSlots.BookingSlot

  @create_attrs %{
    datetime: ~D[2010-04-17 14:00:00],
  }
  @update_attrs %{
    datetime: ~D[2011-05-18 14:00:00],
  }
  @invalid_attrs %{datetime: nil}

  def fixture(:booking_slot) do
    {:ok, booking_slot} = BookingSlots.create_booking_slot(@create_attrs)
    booking_slot
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all booking_slots", %{conn: conn} do
      conn = get(conn, Routes.booking_slot_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create booking_slot" do
    test "renders booking_slot when data is valid", %{conn: conn} do
      conn = post(conn, Routes.booking_slot_path(conn, :create), booking_slot: @create_attrs)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, Routes.booking_slot_path(conn, :show, id))

      assert %{
               "id" => id,
               "date" => "2010-04-17",
               "time_slot" => "14:00:00"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.booking_slot_path(conn, :create), booking_slot: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update booking_slot" do
    setup [:create_booking_slot]

    test "renders booking_slot when data is valid", %{conn: conn, booking_slot: %BookingSlot{id: id} = booking_slot} do
      conn = put(conn, Routes.booking_slot_path(conn, :update, booking_slot), booking_slot: @update_attrs)
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.booking_slot_path(conn, :show, id))

      assert %{
               "id" => id,
               "date" => "2011-05-18",
               "time_slot" => "15:01:01"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, booking_slot: booking_slot} do
      conn = put(conn, Routes.booking_slot_path(conn, :update, booking_slot), booking_slot: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete booking_slot" do
    setup [:create_booking_slot]

    test "deletes chosen booking_slot", %{conn: conn, booking_slot: booking_slot} do
      conn = delete(conn, Routes.booking_slot_path(conn, :delete, booking_slot))
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, Routes.booking_slot_path(conn, :show, booking_slot))
      end
    end
  end

  defp create_booking_slot(_) do
    booking_slot = fixture(:booking_slot)
    %{booking_slot: booking_slot}
  end
end
