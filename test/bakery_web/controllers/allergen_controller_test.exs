defmodule BakeryWeb.AllergenControllerTest do
  use BakeryWeb.ConnCase

  alias Bakery.Allergens
  alias Bakery.Allergens.Allergen

  @create_attrs %{
    image: "some image",
    name: "some name"
  }
  @update_attrs %{
    image: "some updated image",
    name: "some updated name"
  }
  @invalid_attrs %{image: nil, name: nil}

  def fixture(:allergen) do
    {:ok, allergen} = Allergens.create_allergen(@create_attrs)
    allergen
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all allergens", %{conn: conn} do
      conn = get(conn, Routes.allergen_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create allergen" do
    test "renders allergen when data is valid", %{conn: conn} do
      conn = post(conn, Routes.allergen_path(conn, :create), allergen: @create_attrs)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, Routes.allergen_path(conn, :show, id))

      assert %{
               "id" => id,
               "image" => "some image",
               "name" => "some name"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.allergen_path(conn, :create), allergen: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update allergen" do
    setup [:create_allergen]

    test "renders allergen when data is valid", %{conn: conn, allergen: %Allergen{id: id} = allergen} do
      conn = put(conn, Routes.allergen_path(conn, :update, allergen), allergen: @update_attrs)
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.allergen_path(conn, :show, id))

      assert %{
               "id" => id,
               "image" => "some updated image",
               "name" => "some updated name"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, allergen: allergen} do
      conn = put(conn, Routes.allergen_path(conn, :update, allergen), allergen: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete allergen" do
    setup [:create_allergen]

    test "deletes chosen allergen", %{conn: conn, allergen: allergen} do
      conn = delete(conn, Routes.allergen_path(conn, :delete, allergen))
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, Routes.allergen_path(conn, :show, allergen))
      end
    end
  end

  defp create_allergen(_) do
    allergen = fixture(:allergen)
    %{allergen: allergen}
  end
end
