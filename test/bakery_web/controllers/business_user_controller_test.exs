defmodule BakeryWeb.BusinessUserControllerTest do
  use BakeryWeb.ConnCase

  alias Bakery.BusinessUsers
  alias Bakery.BusinessUsers.BusinessUser

  @create_attrs %{
    email: "some email",
    hashed_password: "some hashed_password"
  }
  @update_attrs %{
    email: "some updated email",
    hashed_password: "some updated hashed_password"
  }
  @invalid_attrs %{email: nil, hashed_password: nil}

  def fixture(:business_user) do
    {:ok, business_user} = BusinessUsers.create_business_user(@create_attrs)
    business_user
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all business_users", %{conn: conn} do
      conn = get(conn, Routes.business_user_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create business_user" do
    test "renders business_user when data is valid", %{conn: conn} do
      conn = post(conn, Routes.business_user_path(conn, :create), business_user: @create_attrs)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, Routes.business_user_path(conn, :show, id))

      assert %{
               "id" => id,
               "email" => "some email",
               "hashed_password" => "some hashed_password"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.business_user_path(conn, :create), business_user: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update business_user" do
    setup [:create_business_user]

    test "renders business_user when data is valid", %{conn: conn, business_user: %BusinessUser{id: id} = business_user} do
      conn = put(conn, Routes.business_user_path(conn, :update, business_user), business_user: @update_attrs)
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.business_user_path(conn, :show, id))

      assert %{
               "id" => id,
               "email" => "some updated email",
               "hashed_password" => "some updated hashed_password"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, business_user: business_user} do
      conn = put(conn, Routes.business_user_path(conn, :update, business_user), business_user: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete business_user" do
    setup [:create_business_user]

    test "deletes chosen business_user", %{conn: conn, business_user: business_user} do
      conn = delete(conn, Routes.business_user_path(conn, :delete, business_user))
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, Routes.business_user_path(conn, :show, business_user))
      end
    end
  end

  defp create_business_user(_) do
    business_user = fixture(:business_user)
    %{business_user: business_user}
  end
end
